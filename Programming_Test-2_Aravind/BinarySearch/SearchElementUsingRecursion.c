/*
 *
 *	This program is for searching an element in a sorted array using Recursive call.
 */

#include <stdio.h>
#include <stdlib.h>

//Function prototype
int  BinarySearch(int,int,int*,int);

int main(void) {

	int Array[] = {3,5,7,9,10,14,16,35,67,89,90};
	
	//calculating the given size of array
	int size =  sizeof(Array)/sizeof(Array[0]);

	int position = 0,data;
	printf("Size of Array : %d\n",size);

	puts("Enter the data Wants to be Search");
	scanf("%d",&data);

	position = BinarySearch(0,size,Array,data);
	
	if( position == -1 ) {
		puts("Given Data is Not presented");
	} else {
		printf("Given data :' %d' id presented at position : %d\n",data,position+1);
	}


	return 0;
}

/*
 *	 Functions for searching an element is presented in a array by using recursion.
 */
int  BinarySearch(int lowVal,int highVal,int array[],int data) {


	int middle;
	if(lowVal > highVal)
		return -1;
	middle = (lowVal+highVal) / 2;
	
	if(array[middle] ==  data) {
		
	//	printf("Given data :' %d' id presented at position : %d\n",data,middle+1);
		return middle;
	}

	if(array[middle] > data) {

		middle = middle-1;
		BinarySearch(lowVal,middle,array,data);
		
	}
	else {
		middle = middle + 1;
		BinarySearch(middle,highVal,array,data);
	}
}
