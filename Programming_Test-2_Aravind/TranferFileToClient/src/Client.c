/*
 * Client program is for asking the file to server and receive data if it is presented.
 */
#include "headers.h"

int main(int argc, char const *argv[])
{ 
    int sockfd = 0;

    struct sockaddr_in serv_addr; 
   
    char *ptr = NULL; 
    char buffer[SIZE] = {0}; 
    //checking the command line arguments
    if(argc < 3) {
     	puts("Invalid arguments you are provided");
 	exit(1);
    }	
    //creating a sockfdet
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 
    puts("Socket is Successfully created"); 
    memset(&serv_addr, '0', sizeof(serv_addr)); 
    
    //AF_INET is for ipv4  
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(atoi(argv[2])); 
    serv_addr.sin_addr.s_addr = inet_addr (argv[1]);       
   
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        printf("\nConnection Failed \n"); 
        return -1; 
    }
    puts("Connection Established");
	  puts("Enter the Message :");
	  if((ptr = (char *)malloc(sizeof(char)*100)) == NULL) {
		  perror("malloc");
		  exit(1);
	  } else {
		fgets(ptr,sizeof(ptr),stdin);
 	  	send(sockfd , ptr , strlen(ptr) , 0 ); 
  	  	printf("Message sent\n");
	        free(ptr);
	  }
	  recv(sockfd,buffer,SIZE,0);
	  if(!strcmp(buffer,"FILE NOT FOUND")) 
  	  	printf("%s\n",buffer );
	  else {
		  FILE *fp;
		  fp = fopen("temp.txt","w");
		  if (fp == NULL) {
			 perror("fopen");
			 exit(1);
		  }
		  else 
			  fwrite(buffer,SIZE,1,fp);
		  fclose(fp);
	  }

	  memset(buffer,'0',SIZE);

    return 0; 
}
