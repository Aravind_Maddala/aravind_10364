
/*
 *	This program is for sending a file to the client which is required.
 *
 */


#include "headers.h"

int main(int argc, char const *argv[]) 
{   // Pointer for directory entry 
       	struct dirent *de; 
	int flag=0;
	FILE *fp;
	char buffer[SIZE];
	char filename[FILESIZE]="LsCommand.c"; 
	int server_fd,new_socket; 
	struct sockaddr_in address; 
    	int addrlen = sizeof(address);

	if(argc < 2) {
		printf("Invalid arguments");
		exit(1);
	}

	// Creating socket file descriptor 
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
	{ 
		perror("socket failed"); 
		exit(EXIT_FAILURE); 
	} 
	address.sin_family = AF_INET; 
	address.sin_addr.s_addr = inet_addr("0.0.0.0"); 
	address.sin_port = htons(atoi(argv[1])); 

	// Forcefully attaching socket to the port  
	if (bind(server_fd, (struct sockaddr *)&address,  
				sizeof(address))<0) 
	{ 
		perror("bind failed"); 
		exit(EXIT_FAILURE); 
	}

	if (listen(server_fd, 3) < 0) 
	{ 
		perror("listen"); 
		exit(EXIT_FAILURE); 
	} 
	puts("Waiting for request"); 
	new_socket = accept(server_fd, (struct sockaddr *)&address,(socklen_t*)&addrlen);
	if( new_socket < 0 ) { 
		perror("accept"); 
		exit(EXIT_FAILURE); 
	}
	else {
		printf("Connection is Esatablished\n");

		read(new_socket,filename,FILESIZE);
	
		// opendir() returns a pointer of DIR type.  
		DIR *dr = opendir("../etc/");
		if (dr == NULL)  // opendir returns NULL if couldn't open directory 
		{
			printf("Could not open current directory" );
			return 0;
		}

		// for readdir() 
		while ((de = readdir(dr)) != NULL) {
			
			if(!strcmp(de->d_name,"LsCommand.c")) {
				printf("File Found\n");
				memset(buffer,'\0',SIZE);

				//open the founded file in readonly mode
				fp = fopen(de->d_name,"r");  
				if(fp == NULL) {
					perror("fopen");
					return 1;
				}
				fread(buffer,SIZE,1,fp);
			        if(buffer==NULL)	{
					perror("fread");
				}
				write(new_socket,buffer,SIZE);
			     //	puts(buffer);
				fclose(fp);
				flag=1;
				break;
			}
			else
				continue;
				
		}
		if(flag==0)
			write(new_socket,"FILE NOT FOUND",SIZE);
		closedir(dr);
	}
	return 0; 
} 

