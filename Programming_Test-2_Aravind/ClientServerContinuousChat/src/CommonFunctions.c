/*
 *	These are the common functions for both client and server.
 */
#include "headers.h"


//global variables
char buffer[SIZE];
char buf[4] = {'b','y','e','\n'};


/*
 * fuction definition for read the data 
 * And monitoring the sokcet till the data is presented and read it into the buffer.
 *
 */
void * readData(void *arg) {

  while(1) {
	memset(buffer,'\0',SIZE);
        int  newSocket = *(int *) arg;
        read(newSocket,buffer,SIZE);
        if(!(strcmp(buffer,buf)))
	         exit(0);
	else
            write(1,buffer,SIZE);
 }
        return NULL;
}

/*
 * defining write Data function
 * And write the data into the buffer and send it into the socket id.
 *
 */
void * writeData(void *arg) {
  while(1) {
	memset(buffer,'\0',SIZE);
        int * newSocket = (int *) arg;
        read(0,buffer,SIZE);
        if(!(strcmp(buffer,buf))) {
            	write(*newSocket,buffer,SIZE);
	        exit(0);
	}
	else
            write(*newSocket,buffer,SIZE);
        printf("Message sent\n");
  }
        return NULL;
}

