/**************************************************************
	Aravind Maddala  Emp id:10364	cell No:9959989165
	----------------

	purpose:
	---------------
	program to perform the operations of queue FIFO.

**************************************************************/
#include <iostream>
#include <cstdlib>
#include <string.h>

using namespace std;


class Queue {

private :
	struct List {
		
	   int data;
	   //create self referential pointer 
	   struct List *next;
	};
	struct List *head=NULL;
	int length;
public:
	void enQueue(int);
	void deQueue();
	void displayQueue();
};
/*
 Method for  Enqueue the data into the Queue
*/
void Queue :: enQueue (int data) {
    
    List *newNode = new List;
    List *temp; //temp for traversing 
    length++;
    
    if(newNode == NULL) {
    	cout << "Unable to Allocate Memory:"<<endl;
    }
        
    else {
        newNode->data = data; // Link the data part
        newNode->next = NULL; 
        if( head == NULL) {
	   head = newNode;
	}
        else {
	  temp = head;

          // Traverse to the last node
          while(temp->next != NULL)
              temp = temp->next;

          temp->next = newNode; // Link address part

          cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
	}
    }

}
/*
 Method for dequeue the data into the queue.

*/

void Queue :: deQueue() {
	
	List *temp;
	temp = head;
	if(head == NULL) {
	   cout << "The stack is Empty"<<endl;
	}
	head = head -> next;
	delete temp;
	length--;
	cout << "DEQUEUE DATA SUCCESSFULLY"<<endl;
} 
/*
 Method for display the data which is presented in queue.

*/

void Queue :: displayQueue() {

	List *temp;
	temp = head;
	if (head == NULL) {
		cout <<"List is Empty"<<endl;
		exit(0);
	}
	while ( temp -> next != NULL ) {
	   
	   cout << temp -> data;
	   temp = temp -> next;
	   cout << "--->";
	}
	cout << temp ->data;
	cout<<endl<<endl;

}

int main(void) {

	Queue Node;
	do {
	  int choice;
	  cout << "***********************"<<endl;
	  cout << "1.ENQUEUE\n2.DEQUEUE\n3.Display\n4.Exit"<<endl;
	  cout << "***********************"<<endl;
	  cout << "Enter the choice to preform the operation:"<<endl;
	  cin >> choice;	

	  switch(choice) {
	     case 1 : {
			 cout << "Enter the data to Enqueue :"<<endl;
			 int data;
			 cin >> data;
			 Node.enQueue(data);
		      }
		      break;
	     case 2 : Node.deQueue();
		      break;
	     case 3 : Node.displayQueue();
		      break;
	     case 4 : exit(0);
	     default: cout <<"Enter Valid Choice"<<endl;
          }
	} while(1);
	return 0;
}
/***************************************************
output:
**********************
1.ENQUEUE
2.DEQUEUE
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
3
1--->2--->2--->3--->4--->5--->6

***********************
1.ENQUEUE
2.DEQUEUE
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
2
DEQUEUE DATA SUCCESSFULLY
***********************
1.ENQUEUE
2.DEQUEUE
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
3
2--->2--->3--->4--->5--->6

***********************
1.ENQUEUE
2.DEQUEUE
3.Display
4.Exit
***********************


**************************************************/
