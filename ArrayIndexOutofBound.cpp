/***************************************************************

	Author:
	--------------------
	Aravind Maddala 	Emp id:10364     Cell NO:9959989615
	--------------------
	
	Purpose:
 	--------------------

	Program for Array out of Bound exception 
	whenever the given range is exceed the range of the original 
	array it will give an exception.
****************************************************************/

#include<iostream> 
#include<cstdlib> 
#include<exception>

using namespace std; 

//deriving a class from exception class
class ArrayIndexOutOfBound: public exception{	
	public:
		ArrayIndexOutOfBound() {

			cout<<"Array out of bound Exeception"<<endl;
			exit(0);
		}
		 

};

// A class to represent an integer array 
class Array { 
	private: 
		int *ptr; 
		int size; 
	public: 
		Array(int *, int); 
		// Overloading [] operator to access elements in array style 
		int &operator[] (int); 
		// Utility function to print contents 
		void print(); 
}; 

//overridding the subscript operator 
int &Array::operator[](int index) { 
	try {
	if (index >= size) { 
		cout<<index<<" : is Not in the Range"<<endl;
		throw ArrayIndexOutOfBound(); 
		exit(0); 
	} 
	} catch(exception& e) {
		e.what();
	}
	cout<<"Data is modified at position : "<<index<<endl;
	return ptr[index]; 
} 

// constructor for array class 
Array::Array(int *p = NULL, int s = 0) { 
	size = s; 
	ptr = NULL; 
	if (size != 0) { 
		ptr = new int[size]; 
		for (int i = 0; i < size; i++) 
			ptr[i] = p[i]; 
	} 
} 

//function for display  
void Array::print() { 
	for(int i = 0; i < size; i++) 
		cout<<ptr[i]<<" "; 
	cout<<endl; 
} 

// Driver program to test above methods 
int main(void) { 
	int a[] = {1, 2, 4, 5}; 
	Array arr1(a, 4); 
	arr1[2] = 6; 
	arr1.print(); 
	arr1[8] = 6; 
	return 0; 
	
}

/***********************************************************
OUTPUT:

Data is modified at position : 2
1 2 6 5 
8 : is Not in the Range
Array out of bound Exeception

***********************************************************/ 
