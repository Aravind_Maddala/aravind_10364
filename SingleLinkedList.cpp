/*********************************************************************************
 * Author:
 * -------------
 *  Aravind Maddala	   Emp id:10364		cell NO:9959989615
 *
 *  purpose:
 *  --------------
 *  This program is for creating an single Linked list and do perform some 
 *  operations on it.
 *
 *********************************************************************************/

#include <iostream>
#include <cstdlib>
#include <string.h>
using namespace std;

class SingleLinkedList {

private:
	struct List {
		
	   int data;
	   //create self referential pointer 
	   struct List *next;
	};
	
	struct List *head=NULL;
	int length;	
public:
/*	SingleLinkedList() {

 	head = NULL;

	}
*/	void addAtBegine(int);
	void addAtEnd(int);
	void deleteaNodeAt_Position(int);
	void displayLinkedList();
	void insertNodeAtMiddle(int , int);
	void searchaNode(int );
};
/*
 *  Methode for adding a Node at begining of a linkedlist
 */
void SingleLinkedList :: addAtBegine(int data) {

        struct List *tmp = new List;
	length++;
        if(tmp == NULL) {
           cout << "Memory allocation is failed:"<<endl;
	}
	else  {
          tmp->data = data; // Link data part
          tmp->next = head; // Link address part

          head = tmp;          // Make newNode as first node

           cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
        }      

}	
/*
 *  Method for adding a Node at the end of the singleLinked list
 */
void SingleLinkedList :: addAtEnd(int data) {

    List *newNode = new List;
    List *temp; //temp for traversing 
    length++;
    if(newNode == NULL) {
    	cout << "Unable to Allocate Memory:"<<endl;
    }
    else {
        newNode->data = data; // Link the data part
        newNode->next = NULL; 

        temp = head;

        // Traverse to the last node
        while(temp->next != NULL)
            temp = temp->next;

        temp->next = newNode; // Link address part

        cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
    }
}
/*
 * Method for deleting a Node at specific position.
 */
void SingleLinkedList:: deleteaNodeAt_Position(int position) {
	
	if(position < 0 || position > length) {
           cout<<"Position is greater than List : "<<position<<endl;
	   exit(0);
	} 
	else if(position == 1) {
	  List *temp = head;
	  head = head->next;
	  delete temp;
	  length--;
	}
	else {
          int index=1;
	  List *currentnode=head;
	  List *previousnode=head;
	  while ( index != position ) {
		previousnode = currentnode;
		currentnode = currentnode -> next;
		index=index+1;
	  }
	  previousnode -> next = currentnode -> next;
	  delete currentnode;
	  length--;
	} 
}
/*
 *  Method for display the data which is having a LinkedList.
 */
void SingleLinkedList :: displayLinkedList() {

	List *temp;
	temp = head;
	if (head == NULL) {
		cout <<"List is Empty"<<endl;
		exit(0);
	}
	// iterating the list till n-1 
	while ( temp->next != NULL ) {
	   
	   cout << temp -> data;
	   temp = temp -> next;
	   cout << "--->";
	}

	cout << temp ->data;
	cout<<endl<<endl;

}
/*
 *  This Method is for inserting a Node At middle of a Linked List.
 */
void SingleLinkedList :: insertNodeAtMiddle(int data, int position) {
    struct List *mktemp=NULL;

    struct List *newNode = new List();

    if(newNode == NULL) {
        printf("Unable to allocate memory.");
    }
    else {
        newNode->data = data; // Link data part
        newNode->next = NULL;

        mktemp = head;

        /* 
         * Traverse to the n-1 position 
         */
        for(int i=2; i<=position-1; i++) {
            mktemp = mktemp->next;

            if(mktemp == NULL)
                break;
        }

        if(mktemp != NULL) {
            // Link address part of new node 
            newNode->next = mktemp->next; 

            // Link address part of n-1 node 
            mktemp->next = newNode;

            printf("DATA INSERTED SUCCESSFULLY\n");
        }
        else {
            printf("UNABLE TO INSERT DATA AT THE GIVEN POSITION\n");
        }
    }
}
/*
 * This Method is for searching a Node in a linked List.
 */

void SingleLinkedList :: searchaNode(int data) {
	if ( head == NULL) {
		cout << "The List is empty"<<endl;
		return;
	}
	else {
	   struct List *temp = head;
	   int position = 0; // it indicates the position of data
	   int flag=0;
	   while( temp != NULL ) {
		position++;
		if( temp->data == data) {
		  cout<<"Data is presented at : "<<position<<"-position"<<endl;
		  flag=1;
		  break;
		}
		else { 
		  temp = temp->next;
		}
 	   }
	   if(flag==0)
	       cout << "Data is Not presented in This List:"<<endl;
	}
}
/*
 *  start up fuction.
 */
int main(void) {
	
	// Create a instance of SingleLinkedList class
	SingleLinkedList list;
	do {
	  int choice;
	  cout << "\t***********************"<<endl;
	  cout << "\t1.Add At Begine\n\t2.Add At End\n\t3.Delete A Node\n\t4.Display\n \
	5.Inserting a Node at Middle\n\t6.Searching for a Node\n\t7.Exit"<<endl;
	  cout << "\t***********************"<<endl;
	  cout << "Enter the choice to preform the operation:"<<endl;
	  cin >> choice;
	
         switch(choice) {
	
	   case 1 : { 	cout << " Enter the data want to add:";
			int data;
			cin >> data;
			list.addAtBegine(data);
		    }
		    break;
	
	   case 2 : {   cout << " Enter the data want to add:";
			int data;
			cin >> data;
			list.addAtEnd(data);
		    }
		    break;
	   case 3 : {
			cout << " Enter the Position of Node to delete :";
			int position;
			cin >> position;
			list.deleteaNodeAt_Position(position);
		    }
		    break;
	   case 4 : 	cout << " Display the Linked List :"<<endl;
			list.displayLinkedList();	
			break;
	   case 5 :  {  cout <<"Enter the data :"<<endl;
			int data,position;
			cin>>data;
			cout << "Enter the position :"<<endl;
			cin>>position;
			list.insertNodeAtMiddle(data,position);
		     }
		     break;
	   case 6 : {
			cout<<"Enter the data wants to search:"<<endl;
			int data;
			cin>>data;
			list.searchaNode(data);
		    }
		    break;
	   case 7 : exit(0);
		
	   default :  cout <<" Enter valid choice :"<<endl;
	
	 }
	
	}while(1);

        return 0;

}
