/*
 *  program for finding the substring in Main string | user defined strstr program.
 */
#include <stdio.h>
#include <string.h>
char * userstrstr(char*,char*);
int main(void) {

	char str1[20],str2[10],*ptr;
	printf("Enter the 1st string :");
	scanf("%s",str1);
	printf("Enter the 2st string :");
	scanf("%s",str2);
	if(userstrstr(str1,str2))
		printf("Sub string found\n");
	else
		printf("String Not found\n");

	return 0;
}
char * userstrstr(char *str1,char*str2) {

	char i,j;
	int len,flag=0;
	len = strlen(str1);
	for(i=0 ; i<len ; i++) {
		if(str1[i] == str2[0]) {
			for(j=1;str2[j];j++) {
				if(str1[i+j] == str2[j]) {
					flag=1;
					continue;
				}
				else {
				       flag=0;	
					break;
				}
			}
		}
	}
	if(flag == 1)
		return i;
	else
		return 0;
}
