/************************************************************
  Aravind Maddala  Emp-Id:10364   dvsaravindd@gmail.com  cell NO:9959989615

This program is to print the string in the reversed order 
Example:
	input: Aravind Maddala
	output: Maddala Aravind

************************************************************/
 

#include<stdio.h>
#include<string.h>

int main(void) {
	char array[80],temp;
	int l,i,j,a,b;
	printf("Enter the string:");
	gets(array);
	l=strlen(array)-1;
	for(i=0,j=l;i<j;i++,j--) {
		temp=array[i];
		array[i]=array[j];
		array[j]=temp;
	}

	for(i=0;array[i];i++,a=0,b=0) {
		a=i;
		while(array[i]!=32) {
			if(array[i]==0)
				break;
			b=i;
			i++;
		}
		for(;a<b;a++,b--) {
			temp=array[a];
			array[a]=array[b];
			array[b]=temp;
		}
	}
	array[i]=0;
	puts(array);
}

/***********************************************************

OUTPUT:

enter the string:HI INNOMINDS
INNOMIDS HI

***********************************************************/
