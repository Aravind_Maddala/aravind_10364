/*
 *  User defined string concatination program.
 */
#include <stdio.h>
#include <string.h>

int main(void) {

	char str1[20],str2[10],*ptr;
	int len,i=0,j=0;
	printf("Enter the string1 :");
	scanf("%s",str1);
	ptr=str1;
	printf("Enter the string2 :");
	scanf("%s",str2);
	len = strlen(str1);
	while(str2[j]) {
		str1[len+i] = str2[j]; 
		j++;
		i++;
	}
	puts(str1);
	return 0;

}
