/*
 *  User defined string copy program.
 */
#include <stdio.h>
#include <string.h>

int main(void) {

	char str1[20];
	char str2[20];
	int i,len;
	printf("Enter string 1 :");
	gets(str1);
	len=strlen(str1);
	printf("String1 = %s\n",str1);
	for(i=0;i<len;i++) {
		str2[i] = str1[i];
	}
	printf("String2 = %s\n",str2);
	return 0;
}
