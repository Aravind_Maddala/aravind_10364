/*************************************************************************************************************
NAME                 EMAILID                       EMPLOYEEID                     PHONE NUMBER
--------------------------------------------------------------------------------------------------------------
ARAVIND MADDALA       dvsaravindd@gmail.com        10364                          9959989615
--------------------------------------------------------------------------------------------------------------
This program is implementation of user defined shell(grep is not implemented)

**************************************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
char * arguments[100];


//reading userfrom the stdin
void readComandFromUser() {
	char *string;
	int i=0;
	string=(char *)malloc(20);
	read(0,string,100);
	string[strlen(string)-1]='\0';
	// Returns first token
	char* token = strtok(string, " |");

	while (token != NULL) {
		arguments[i]=token;
		printf("%s\n",arguments[i]);
		i++;
		token = strtok(NULL, " |");
	}
	arguments[i]=token;

}


int main()
{
	int ret = -1,child_exit_status;
	char userName[10]={'\0'};
	char cwd[1024];			

	//geting user name
	getlogin_r(userName,10);
	userName[strlen(userName)]='@';
	char name[] = "myShell$ ";
	strcat(userName,name);

	while(1)
	{
		//writing our own username@shellname to the STDOUT
		write(1,userName, strlen(userName));
		readComandFromUser();
		//if entered string equals to PWD
		if(!(strcmp(arguments[0],"pwd"))) {

			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}
		//if entered string equals to Exit
		else if(!(strcmp(arguments[0],"exit"))) {

			exit(0);
		}
		//if entered string equals tp cd
		else if(!(strcmp(arguments[0],"cd"))) {

			chdir(arguments[1]);
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}


		//if entered string is any linux command 
		ret = fork();
		if(ret == 0) {

			execvp(arguments[0], arguments);
		}
		else
		{
			wait(&child_exit_status);
		}
	}
	return 0;
}
/***********************************output of the above program***************************************
aravind@myShell$ ls -lrt
ls
-lrt
total 20
-rwxr-xr-x 1 aravind aravind 13128 Feb 22 18:37 a.out
-rw-rw-r-- 1 aravind aravind  2419 Feb 22 18:37 myshell.c
aravind@myShell$ pwd
pwd
/home/aravind/C/TrainingatHYD/22-Feb
aravind@myShell$ cal
cal
   February 2019      
Su Mo Tu We Th Fr Sa  
                1  2  
 3  4  5  6  7  8  9  
10 11 12 13 14 15 16  
17 18 19 20 21 22 23  
24 25 26 27 28        
                      
aravind@myShell$ ps -ef
ps
-ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 08:52 ?        00:00:08 /sbin/init splash
root         2     0  0 08:52 ?        00:00:00 [kthreadd]
root         4     2  0 08:52 ?        00:00:00 [kworker/0:0H]
root         7     2  0 08:52 ?        00:00:00 [mm_percpu_wq]
root         8     2  0 08:52 ?        00:00:00 [ksoftirqd/0]
root         9     2  0 08:52 ?        00:00:07 [rcu_sched]
root        10     2  0 08:52 ?        00:00:00 [rcu_bh]
root        11     2  0 08:52 ?        00:00:00 [migration/0]
root        12     2  0 08:52 ?        00:00:00 [watchdog/0]
root        13     2  0 08:52 ?        00:00:00 [cpuhp/0]
root        14     2  0 08:52 ?        00:00:00 [cpuhp/1]
root        15     2  0 08:52 ?        00:00:00 [watchdog/1]
root        16     2  0 08:52 ?        00:00:00 [migration/1]
root        17     2  0 08:52 ?        00:00:00 [ksoftirqd/1]
root        19     2  0 08:52 ?        00:00:00 [kworker/1:0H]
root        20     2  0 08:52 ?        00:00:00 [cpuhp/2]
root        21     2  0 08:52 ?        00:00:00 [watchdog/2]
root        22     2  0 08:52 ?        00:00:00 [migration/2]
root        23     2  0 08:52 ?        00:00:00 [ksoftirqd/2]

************************************************************************************/

