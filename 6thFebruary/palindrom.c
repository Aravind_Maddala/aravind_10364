/*
 * program to check whether given number is palindrom are not.
 * using both array format  and reverse the number format.
 *
 */
#include <stdio.h>

//declaration of functions
void usingRevOperation(int);
int usingArray(int); 

int main(void) {

   int num;
   printf("Enter the Number:");
   scanf("%d",&num);
   printf("Enter the choice:");
   printf("1.using Array 2.using Reverse the value\n");
   int choice;
   scanf("%d",&choice);
   switch(choice) {
	   case 1:usingRevOperation(num);
		  break;
	   case 2:
		  if(usingArray(num)!=0)
			  printf("is a palindrom\n");
		  else
			  printf("Not a palindrom\n");
		  break;
	  default: printf("Invalid choice\n");
   }

   return 0;
}
//function definition using Array method
int usingArray(int num) {
	int array[10],i=0,temp=num;
	while(temp!=0) {
		array[i] = temp % 10;
		i++;
		temp= temp / 10;	
	}
	int len = sizeof(array)/sizeof(array[0]);
	for(i=0;i<(len/2);i++) {
		if(array[i] == array[len-i])
			continue;
		else
			return -1;
	}
	return 0;
}


//definition of Reverse the number method.
void usingRevOperation(int num) {
   int temp,result=0;
   temp = num;
   while(temp!=0) {
	result = result*10 +(temp%10);
	temp = temp /10;
   }
   if(result != num) 
	   printf("This is Not a palindrome\n");
   else
	   printf("This is a Palindrome Number\n");
}
