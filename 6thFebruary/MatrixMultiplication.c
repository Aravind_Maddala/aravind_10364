/*----------------------------------------------------------------------------------------------------------------------------------
 M D V S ARAVIND	10364	dvsaravindd@gmail.com		Cell : 9959989615

  In this program we perform the multiplication of two matrices  only if columns of first matrix equal to 
  rows of second matrix.
  ------------------------------------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

//display eleemts in the matrix
void printMatrix(int rows,int columns,int matrix[][columns]) {

	for(int i=0;i<rows;i++){
		for(int j=0;j<columns;j++){
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}
}

int main() {

	int row,column,sum,row1,column1;


	printf("enter how many rows in a matrix1 \n");
	scanf("%d",&row);
	while(row<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row);
	}
	printf("enter how many columns in a matrix1 \n");
	scanf("%d",&column);
	while(column<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column);
	}

	printf("enter how many rows in a matrix2 \n");
	scanf("%d",&row1);
	while(row1<=0) {
		printf("in a matrix rows cannot be negative or 0,please re enter \n");
		scanf("%d",&row1);
	}
	printf("enter how many columns in a matrix2 \n");
	scanf("%d",&column1);
	while(column1<=0) {
		printf("in a matrix column cannot be negative or 0,please re enter \n");
		scanf("%d",&column1);
	}


	if(column!=row1) {

		printf("matrix multiplicaton is not possible \n");
		exit(0);
	}

	srand(getpid());
	int matrix[row][column];
	int matrix1[row1][column1];
	int resultMatrix[row][column1];

	for(int i=0;i<row;i++){
		for(int j=0;j<column;j++){
			matrix[i][j]=rand()%10;
		}
	}
	printf("printing the first matrix\n");

	printMatrix(row,column,matrix);

	for(int i=0;i<row1;i++){
		for(int j=0;j<column1;j++){
			matrix1[i][j]=rand()%10;
		}
	}
	printf("printing the second matrix\n");

	printMatrix(row1,column1,matrix1);

	for(int i=0;i<row;i++){
		for(int j=0;j<column1;j++){
			sum=0;
			for(int k=0;k<column;k++){
				sum+=matrix[i][k]*matrix1[k][j];
			}
			resultMatrix[i][j]=sum;
		}
	}

	printf("printing the resultant matrix after multiplication\n");
	printMatrix(row,column1,resultMatrix);

	return 0;	
}

/*******************************************OUTPUT ************************************
  enter how many rows in a matrix1 
  3
  enter how many columns in a matrix1 
  4
  enter how many rows in a matrix2 
  4
  enter how many columns in a matrix2 
  3
  printing the first matrix
  4 1 4 3 
  4 5 4 6 
  6 4 2 6 
  printing the second matrix
  5 3 0 
  8 8 4 
  8 3 0 
  1 3 3 
  printing the resultant matrix after multiplication
  63 41 13 
  98 82 38 
  84 74 34  
 **********************/
