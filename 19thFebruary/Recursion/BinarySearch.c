/***************************************************************************************

 ARAVIND MADDALA     10364       dvsaravindd@gmail.com    cell:9959989615 

PURPOSE:
	~ this program is to find an element in the given array using binary search function with using recursion concept
*****************************************************************************************/

#include<stdio.h>
#include<stdlib.h>

//it search the element in the given array
int binarysearch(int low,int high,int *array,int data) {

	int mid;

	if(low>high)
		return -1;
	mid=(low+high)/2;

	if(array[mid]==data)
		return mid;

	else if(array[mid]>data) {
		high = mid-1;
		binarysearch(low,high,array,data);
	}
	else {
		low=mid+1;
		binarysearch(low,high,array,data);
	}

}
//it sorts array in ascending order
int * sorting(int *array,int size) {

	int i,j,temp;

	for(i=0;i<size;i++) {
		for(j=0;j<size-1;j++) {
			if(array[j] > array[j+1]) {
				temp = array[j];
				array[j]=array[j+1];
				array[j+1]=temp;
			}
		}
	
	}

	return array;
}
int main() {
	int array[100];
	int size,i,data,index;
	printf("enter the size of array\n");
	scanf("%d",&size);
	while(size<=0) {
		printf("size cannot be negative.....re enter\n");
		scanf("%d",&size);
	}
	printf("enter the elemts into array \n");
	for(i=0;i<size;i++) {
		scanf("%d",&array[i]);
	}

	sorting(array,size);

//the elemts in the array after the sorting are 
	for(i=0;i<size;i++) {
		printf("%d\t",array[i]);
	}
	printf("\n");

	printf("enter the data to be searched\n");
	scanf("%d",&data);

	index=binarysearch(0,size-1,array,data);

	if(index>=0)
		printf("the data  %d is found at index  %d  in the given array \n",data,index);
	else
		printf("the data %d  is not found in the given array \n",data);
	return 0;


}

/*********************************OUTPUT*************************************
enter the size of array
5
enter the elemts into array 
1
2
3
0
8
0	1	2	3	8	
enter the data to be searched
8
the data  8 is found at index  4  in the given array 
*****************************************************************************/
