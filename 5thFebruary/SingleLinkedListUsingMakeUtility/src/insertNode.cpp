#include <headers.h>
/*
 *  Methode for adding a Node at begining of a linkedlist
 */
void SingleLinkedList :: addAtBegine(int data) {

        struct List *tmp = new List;
	length++;
        if(tmp == NULL) {
           cout << "Memory allocation is failed:"<<endl;
	}
	else  {
          tmp->data = data; // Link data part
          tmp->next = head; // Link address part

          head = tmp;          // Make newNode as first node

           cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
        }      

}	
/*
 *  Method for adding a Node at the end of the singleLinked list
 */
void SingleLinkedList :: addAtEnd(int data) {

    List *newNode = new List;
    List *temp; //temp for traversing 
    length++;
    if(newNode == NULL) {
    	cout << "Unable to Allocate Memory:"<<endl;
    }
    else {
        newNode->data = data; // Link the data part
        newNode->next = NULL; 

        temp = head;

        // Traverse to the last node
        while(temp->next != NULL)
            temp = temp->next;

        temp->next = newNode; // Link address part

        cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
    }
}

/*
 *  This Method is for inserting a Node At middle of a Linked List.
 */
void SingleLinkedList :: insertNodeAtMiddle(int data, int position) {
    struct List *mktemp=NULL;

    struct List *newNode = new List();

    if(newNode == NULL) {
        printf("Unable to allocate memory.");
    }
    else {
        newNode->data = data; // Link data part
        newNode->next = NULL;

        mktemp = head;

        /* 
         * Traverse to the n-1 position 
         */
        for(int i=2; i<=position-1; i++) {
            mktemp = mktemp->next;

            if(mktemp == NULL)
                break;
        }

        if(mktemp != NULL) {
            // Link address part of new node 
            newNode->next = mktemp->next; 

            // Link address part of n-1 node 
            mktemp->next = newNode;

            printf("DATA INSERTED SUCCESSFULLY\n");
        }
        else {
            printf("UNABLE TO INSERT DATA AT THE GIVEN POSITION\n");
        }
    }
}
