#include <headers.h>
/*
 * This Method is for searching a Node in a linked List.
 */

void SingleLinkedList :: searchaNode(int data) {
	if ( head == NULL) {
		cout << "The List is empty"<<endl;
		return;
	}
	else {
	   struct List *temp = head;
	   int position = 0; // it indicates the position of data
	   int flag=0;
	   while( temp != NULL ) {
		position++;
		if( temp->data == data) {
		  cout<<"Data is presented at : "<<position<<"-position"<<endl;
		  flag=1;
		  break;
		}
		else { 
		  temp = temp->next;
		}
 	   }
	   if(flag==0)
	       cout << "Data is Not presented in This List:"<<endl;
	}
}

/*
 *  Method for display the data which is having a LinkedList.
 */
void SingleLinkedList :: displayLinkedList() {

	List *temp;
	temp = head;
	if (head == NULL) {
		cout <<"List is Empty"<<endl;
		exit(0);
	}
	// iterating the list till n-1 
	while ( temp->next != NULL ) {
	   
	   cout << temp -> data;
	   temp = temp -> next;
	   cout << "--->";
	}

	cout << temp ->data;
	cout<<endl<<endl;

}
