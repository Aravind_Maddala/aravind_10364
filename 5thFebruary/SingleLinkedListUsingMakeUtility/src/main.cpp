#include <headers.h>

/*
 *  start up fuction.
 */
int main(void) {
	
	// Create a instance of SingleLinkedList class
	SingleLinkedList list;
	do {
	  int choice;
	  cout << "\t***********************"<<endl;
	  cout << "\t1.Add At Begine\n\t2.Add At End\n\t3.Delete A Node\n\t4.Display\n \
	5.Inserting a Node at Middle\n\t6.Searching for a Node\n\t7.Exit"<<endl;
	  cout << "\t***********************"<<endl;
	  cout << "Enter the choice to preform the operation:"<<endl;
	  cin >> choice;
	
         switch(choice) {
	
	   case 1 : { 	cout << " Enter the data want to add:";
			int data;
			cin >> data;
			list.addAtBegine(data);
		    }
		    break;
	
	   case 2 : {   cout << " Enter the data want to add:";
			int data;
			cin >> data;
			list.addAtEnd(data);
		    }
		    break;
	   case 3 : {
			cout << " Enter the Position of Node to delete :";
			int position;
			cin >> position;
			list.deleteaNodeAt_Position(position);
		    }
		    break;
	   case 4 : 	cout << " Display the Linked List :"<<endl;
			list.displayLinkedList();	
			break;
	   case 5 :  {  cout <<"Enter the data :"<<endl;
			int data,position;
			cin>>data;
			cout << "Enter the position :"<<endl;
			cin>>position;
			list.insertNodeAtMiddle(data,position);
		     }
		     break;
	   case 6 : {
			cout<<"Enter the data wants to search:"<<endl;
			int data;
			cin>>data;
			list.searchaNode(data);
		    }
		    break;
	   case 7 : exit(0);
		
	   default :  cout <<" Enter valid choice :"<<endl;
	
	 }
	
	}while(1);

        return 0;

}
