#include <headers.h>
/*
 * Method for deleting a Node at specific position.
 */
void SingleLinkedList:: deleteaNodeAt_Position(int position) {
	
	if(position < 0 || position > length) {
           cout<<"Position is greater than List : "<<position<<endl;
	   exit(0);
	} 
	else if(position == 1) {
	  List *temp = head;
	  head = head->next;
	  delete temp;
	  length--;
	}
	else {
          int index=1;
	  List *currentnode=head;
	  List *previousnode=head;
	  while ( index != position ) {
		previousnode = currentnode;
		currentnode = currentnode -> next;
		index=index+1;
	  }
	  previousnode -> next = currentnode -> next;
	  delete currentnode;
	  length--;
	} 
}
