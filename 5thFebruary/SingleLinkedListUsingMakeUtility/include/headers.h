#include <iostream>
#include <cstdlib>
#include <string.h>
using namespace std;

class SingleLinkedList {

private:
	struct List {
		
	   int data;
	   //create self referential pointer 
	   struct List *next;
	};
	
	struct List *head=NULL;
	int length;	
public:
/*	SingleLinkedList() {

 	head = NULL;

	}
*/	void addAtBegine(int);
	void addAtEnd(int);
	void deleteaNodeAt_Position(int);
	void displayLinkedList();
	void insertNodeAtMiddle(int , int);
	void searchaNode(int );
};
