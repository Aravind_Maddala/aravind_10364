/*
 * program to allocate memory for 2d array dynamically using array of pointers.
 */
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int row=4,col=4,i,count=0,j;
	int *arr[row];
	for(i=0 ; i < row; i++) {
	       arr[i] = (int*) malloc (col*sizeof(int));
	}
	
	for(i=0 ; i < row; i++) {
	   for(j=0 ; j < col; j++) {
		   arr[i][j] = ++count;
	   }
	}
	for(i=0 ; i < row; i++) {
	   for(j=0 ; j < col; j++) {
		printf("arr[%d][%d]=%d  ",i,j,arr[i][j]);
	   }
	}
	return 0;
}
