/*
 * the code is for allocating a memory dynamically for 2d arry using single pointer.
 */
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	int row=4,col=4;
	int i,j,count=0;
	int *arr = (int *) malloc (row * col* sizeof(int));

	for(i=0 ; i< row ; i++) {
	   for(j=0 ; j < col ; j++) {
		
		*(arr + i*col + j) = ++count;
	   }
	}	

	for(i=0 ; i< row ; i++) {
	   for(j=0 ; j < col ; j++) {

	   	printf("arr[%d][%d] = %d  ",i,j,*(arr+i*col+j));
	   }
	   printf("\n");
	}
}
