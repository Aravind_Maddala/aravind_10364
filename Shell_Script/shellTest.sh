#!/bin/sh
#This is comment
echo "Hello..."
echo hello * world

#echo VAR =1  # This is wrong assignment: shell thinks VAR is a command and =1 is argument

VAR=1
echo "value of VAR is: " $VAR

MY_STR="Hello World"
echo "content of \$MY_STR is \""$MY_STR\" "\\"

MY_STR="How are you?"
echo "content of \$MY_STR is \""$MY_STR\" "\\"
echo "What is your username?"
#read ANSWER
#echo "I ll create a file for username: "$ANSWER"_file.txt"

#touch $ANSWER.txt
#touch ${ANSWER}_file.txt

echo "word	with 	tabs"

echo "Hello   " World ""


for i in 1 2 3 4 5
do
	echo $i
done

#for i in `seq 100`
#do
#	echo $i
#done
:<<'END'
echo this line is commented
END
echo after comment
echo "**************************"
for i in 1 2 a b c Hello
do
	echo $i
done
echo "**************************"


INPUT_STRING=hello
while [ "$INPUT_STRING" != "bye" ]
do
  echo "Please type something in (bye to quit)"
  read INPUT_STRING
  echo "You typed: $INPUT_STRING"
done

while read f
do
  case $f in
	hello)		echo English	;;
	howdy)		echo American	;;
	gday)		echo Australian	;;
	bonjour)	echo French	;;
	"guten tag")	echo German	;;
	"brk")		break		;;
	*)		echo Unknown Language: $f
		;;
   esac
done


echo enter an integer

read INT

if test $INT -eq 5  #if test $INT = 5  # if [ $INT = 5 ] ; we can use -ne, -eq, -gt, -lt, -ge, -le, 
then
	echo you entered 5
else
	echo wrong number
fi

#-nt : newer than; -ot : older than


echo $0
echo $1
echo $2
echo $@
echo $*
echo "the file is called with "$#" no of parameters"
echo $?   #prints exit status of last called command


echo "What is your name [ `whoami` ] "
read myname
if [ -z "$myname" ]; then
  myname=`whoami`
fi
echo "Your name is : $myname"

#cut, sed, head, tail




