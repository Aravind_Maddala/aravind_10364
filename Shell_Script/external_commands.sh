cat "test.csv"
echo ""
cat "test.csv" | head -4 #prints first four lines
echo ""
cat test.csv | head -4 | tail -2 #prints last two lines
echo ""
cat "test.csv" | cut -d "," -f2-3  # cut the string with delimeter "," and print the fields 2 and 3
cat "test.csv" | head -5 | tail -2| cut -d "," -f2 


echo ""
head -5 "test.csv" | tail -2 |  cut -d "," -f2 >> "replacement.txt"
echo ""
sed -i 's/23/abc/g' "test.csv"

echo "Fine more info on: https://www.geeksforgeeks.org/sed-command-in-linux-unix-with-examples/"

ls -l | grep file_user_2.txt | cut -d " " -f12 #printing the last modification time of "file_user_2.txt"

echo ""
ifconfig | tail -8 | head -1 | cut -d " " -f10 # printing ip address of a system

