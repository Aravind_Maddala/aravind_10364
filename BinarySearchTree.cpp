/*********************************************
   ARAVIND MADDALA     EMP ID:10364   CELL NO:9959989615

   purpose:
   ------------------
   program for creating a binary search tree and display
   the values Inorder,preorder,postorder format.

*********************************************/

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <queue>
#include <iterator>

using namespace std;

struct TreeNode {
  int data;
  TreeNode *left;
  TreeNode *right;
};

class BinaryTree {
	queue<int> myqueue;
public:
	TreeNode* Insert(TreeNode*,int);
	void Inorder(TreeNode *);
	void Preorder(TreeNode *);	
	void Postorder(TreeNode *);
	friend TreeNode * Find(TreeNode *, int);
	void displayQueue();


};

//Method for inserting an node in the tree
TreeNode* BinaryTree :: Insert(TreeNode *node,int data)  {
    if(node==NULL)
    {
        TreeNode *temp;
        temp=new TreeNode;
        temp -> data = data;
        temp -> left = temp -> right = NULL;
	myqueue.push(data);
        return temp;
    }
    if(data >(node->data))
    {
        node->right = Insert(node->right,data);
    }
    else if(data < (node->data))
    {
        node->left = Insert(node->left,data);
    }
    /* Else there is nothing to do as the data is already in the tree. */
    return node;
}

//Method for display the content in Inorderd format
void BinaryTree :: Inorder(TreeNode *node) {
    if(node==NULL)
    {
        return;
    }
    Inorder(node->left);
    cout<<node->data<<" ";
    Inorder(node->right);
}

//Method for display the content in preorderd format
void BinaryTree :: Preorder(TreeNode *node) {
    if(node==NULL)
    {
        return;
    }
    cout<<node->data<<" ";
    Preorder(node->left);
    Preorder(node->right);
}

//Method for display the content in preorderd format
void BinaryTree :: Postorder(TreeNode *node) {
    if(node==NULL)
    {
        return;
    }
    Postorder(node->left);
    Postorder(node->right);
    cout<<node->data<<" ";
}

//Method for searching the element which is presented in tree or not.
TreeNode*  Find(TreeNode *node, int data) {
    if(node==NULL)  {
        /* Element is not found */
        return NULL;
    }
    if(data > node->data) {
        /* Search in the right sub tree. */
        return Find(node->right,data);
    }
    else if(data < node->data) {
        /* Search in the left sub tree. */
        return Find(node->left,data);
    }
    else   {
        /* Element Found */
        return node;
    }
}

void BinaryTree :: displayQueue() {

while(!myqueue.empty()) {
cout<<myqueue.front()<<endl;
myqueue.pop();

}

}

int main(void)  {
    TreeNode *root = NULL,*temp;
    BinaryTree object;
    int choice;
    //clrscr();
    while(1)
    {
        cout<<"\n1.Insert\n2.Inorder\n3.Preorder\n4.Postorder\n5.Search\n6.Exit"<<endl;
        cout<<"Enter your choice:";
        cin>>choice;
        switch(choice) {
          case 1: {
              cout<<"\nEnter element to be insert:";
              cin>>choice;
              root = object.Insert(root, choice);
              cout<<"\nElements in BST are:";
              object.Inorder(root);
	      }
              break;
	  case 2:
              cout<<"\nInorder Travesals is:";
              object.Inorder(root);
              break;
          case 3:
              cout<<"\nPreorder Traversals is:";
              object.Preorder(root);
              break;
          case 4:
              cout<<"\nPostorder Traversals is:";
              object.Postorder(root);
              break;
	  case 5: {
              cout<<"\nEnter element to be searched:";
              cin>>choice;
              temp = Find(root,choice);
               if(temp==NULL) {
                  cout<<"Element is not foundn";
               }
               else {
                  cout<<"Element "<<temp->data<<" is Found\n";
              }
	      }
              break;
	  case 6: {

			object.displayQueue();
		  }
		break;
          case 7:
              exit(0);
              break;
          default:
              cout<<"\nEnter correct choice:";
              break;
        }
      }
}
/***************************************************
OUTPUT:
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:1

Enter element to be insert:8

Elements in BST are:2 3 4 5 6 7 8 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:1

Enter element to be insert:1

Elements in BST are:1 2 3 4 5 6 7 8 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:1

Enter element to be insert:9

Elements in BST are:1 2 3 4 5 6 7 8 9 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:2

Inorder Travesals is:1 2 3 4 5 6 7 8 9 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:3

Preorder Traversals is:5 4 3 2 1 6 7 8 9 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:4

Postorder Traversals is:1 2 3 4 9 8 7 6 5 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:4

Postorder Traversals is:1 2 3 4 9 8 7 6 5 
1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:5

Enter element to be searched:9
Element 9 is Found

1.Insert
2.Inorder
3.Preorder
4.Postorder
5.Search
6.Exit
Enter your choice:


***************************************************/
