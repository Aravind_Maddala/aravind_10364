
/****************************************************************************************************** 
  	
	Aravind Maddala 	 Emp Id:10364	cell No:9959989615	
  	

        Purpose:
        --------

        --To Communicate between server and client using sharedmemory and threads
        --server uses sharedmemmory concept

******************************************************************************************************/



#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>  
#include <stdio.h> 
using namespace std; 

class Server {
	
	int shmid ;
	public :
	//creating default constructor for server
	Server() {

		key_t key = ftok("myfile1",85);
		shmid= shmget(key,1024,0666|IPC_CREAT); 


		char *str = (char *)shmat(shmid,(void*)0,0);  

		gets(str);
		shmdt(str);
	}
	//method to print the message entered by the client
	void printMessage() {

		char *str1 = (char *)shmat(shmid,(void*)0,0);  
		cout<<"HELLO......"<< str1<<endl;
	}
};




int main() 
{ 
	int i=0;
	//creating the instance of server class
	Server s;
	while(i<3) {
		i++;
		sleep(20);
		s.printMessage();
	}
	return 0; 
} 
/*================================================================================
 OUTPUT:
 ----------
 HI
 HELLO......Aravind
 HELLO......Bhaskar
 HELLO......Naidu

*/

