/******************************************************************
	
	Aravind Maddala		Emp id:10364	cell NO:9959989615

	----------------------
	purpose:
	---------------------
	Program for create a single linked list.

******************************************************************/

#include <iostream>
#include <cstdlib>
#include <string.h>
using namespace std;

class SingleLinkedList {

private:
	struct List {
		
	   int data;
	   //create self referential pointer 
	   struct List *next;
	};
	struct List *head=NULL;
	int length;	
public:
	SingleLinkedList() {

 	length = 0;

	}
	void addANode(int,int);
	void deleteaNodeAt_Position(int);
	void displayLinkedList();
	void updateANode(int);
};

/*
	Method for adding a Node at pariticular position
	Must the First Node position should be 0. 

*/
void SingleLinkedList :: addANode(int position,int data) {
	int flag = 0;
	if(position <0 || position > length) {
        	cout << " Invalid position :"<<endl;
		cout << "The length of list is :"<<length<<endl;
		flag =1;
	}
	else if(position==0) {

	   List *newnode=new List;
	   length++;

	   if (head == NULL) {
		head = newnode;
		head->data = data;
		head->next = NULL;
		return;
	   }

	   newnode->data = data;
	   newnode->next = head;
	   head = newnode;
	} 
	else if(position==length) {

		List  *temp;
		List *newnode=new List;
		length++;
		temp=head;
		while (temp->next != NULL)
			temp = temp->next;
			temp->next = newnode;
			newnode->data    = data;
			newnode->next    = NULL;

	}
	else {
		int index=1;
		List *newnode=new List;
		newnode->data = data;
		List *currentNode=head;
		List *prevNode=head;
		while(index !=position) {
		prevNode=currentNode;
		currentNode=currentNode->next;
		index=index+1;
		}
		prevNode->next=newnode;
		newnode->next=currentNode;
		length++;
	}
	if (flag == 0){
		cout << "Added Node Successfully"<<endl;
		cout << "Length :"<<length<<endl;
	
	}


}
/*
   Method for deleting a Node based on particular position

*/
void SingleLinkedList:: deleteaNodeAt_Position(int position) {
	
	if(position < 0 || position > length) {
           cout<<"Position is greater than List : "<<position<<endl;
	   exit(0);
	} 
	else if(position == 1) {
	  List *temp = head;
	  head = head->next;
	  delete temp;
	  length--;
	}
	else {
          int index=1;
	  List *currentnode=head;
	  List *previousnode=head;
	  while ( index != position ) {
		previousnode = currentnode;
		currentnode = currentnode -> next;
		index=index+1;
	  }
	  previousnode -> next = currentnode -> next;
	  delete currentnode;
	  length--;
	} 
}
/*
   Method for display the data which is presented in Linked List.
*/

void SingleLinkedList :: displayLinkedList() {

	List *temp;
	temp = head;
	if (head == NULL) {
		cout <<"List is Empty"<<endl;
		exit(0);
	}
	while ( temp -> next != NULL ) {
	   
	   cout << temp -> data;
	   temp = temp -> next;
	   cout << "--->";
	}
	cout << temp ->data;
	cout<<endl<<endl;

}
/*
  Method for updating the new data in Linked List based on positon.
*/
void SingleLinkedList:: updateANode(int position) {
	
	if(position<0 || position > length) {
           cout<<"cannot update the data at that position:"<<position<<endl;
        }
        int index;
        List *temp = head;
        for(index=1;index<position;index++) {
              temp=temp->next;
        }
        cout<<"enter the data to be update name:"<<endl;
        cin >> temp->data;
}

int main(void) {
	
	// Create a instance of SingleLinkedList class
	SingleLinkedList list;
	do {
	  int choice;
	  cout << "\t***********************"<<endl;
	  cout << "\t1.Add A Node\n\t2.Delete A Node\n\t3.Update Node\n\t4.Display\n\t5.Exit"<<endl;
	  cout << "\t***********************"<<endl;
	  cout << "Enter the choice to preform the operation:"<<endl;
	  cin >> choice;
	
         switch(choice) {
	
	
	   case 1 : {   cout << " Enter the data want to add:";
			int data;
			cin >> data;
			cout << " Enter the position to add data:";
			int position;
			cin >>position;
			list.addANode(position,data);
		    }
		    break;
	   case 2 : {
			cout << " Enter the Position of Node to delete :";
			int position;
			cin >> position;
			list.deleteaNodeAt_Position(position);
		    }
		    break;
	   case 3 : {
			cout << " Enter the Position to Update data:";
			int position;
			cin>>position;	
			list.updateANode(position);
		    }
		    break;
	   case 4 : 
			cout << " Display the Linked List :"<<endl;
			list.displayLinkedList();	
			break;

	   case 5 : exit(0);
		
	   default :  cout <<" Enter valid choice :"<<endl;
	
	 }
	
	}while(1);

        return 0;

}
/*****************************************************************
output:
	***********************
	1.Add A Node
	2.Delete A Node
	3.Update Node
	4.Display
	5.Exit
	***********************
	Enter the choice to preform the operation:
	4
 Display the Linked List :
50--->40--->30--->20--->10

	***********************
	1.Add A Node
	2.Delete A Node
	3.Update Node
	4.Display
	5.Exit
	***********************
	Enter the choice to preform the operation:
	3
 Enter the Position to Update data:3 
enter the data to be update name:
80
	***********************
	1.Add A Node
	2.Delete A Node
	3.Update Node
	4.Display
	5.Exit
	***********************
        Enter the choice to preform the operation:
	4
 Display the Linked List :
50--->40--->80--->20--->10

	***********************
	1.Add A Node
	2.Delete A Node
	3.Update Node
	4.Display
	5.Exit
	***********************

*****************************************************************/
