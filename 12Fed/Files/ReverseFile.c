/*
 *
 * This program is for read the data presented in one file and 
 * store them into another file in reverse order.
 */
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	//initializing file pointers with null
	FILE *fp1=NULL,*fp2=NULL;
	int ch,len;

	if((fp1 = fopen("text3.txt","r")) == NULL) {
		perror("fp1");
		return -1;
	}
	else 
		printf("FILE -> 1 OPENED SUCCESSFULLY\n");

	if((fp2 = fopen("text4.txt","w")) == NULL) {
		perror("fp2");
		return -1;
	}
	else 
		printf("FILE -> 2 OPENED SUCCESSFULLY\n");

	while((ch = fgetc(fp1)) != EOF){
		printf("%c",ch);
	}
	//placing the file pointer at end.
	fseek(fp1,-1,SEEK_END);

	//find the size of file
	len = ftell(fp1);
	printf("%d",len);
	while(len >= 0){
		ch = fgetc(fp1);
		fputc(ch,fp2);
		fseek(fp1,-2,SEEK_CUR);
		len--;
	}	
	printf("\n");
	//close the file pointers.
	fclose(fp1);
	fclose(fp2);
	return 0;
}
