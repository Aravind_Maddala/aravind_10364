/*
 *
 * This program is for display the data which is presented in given file.
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	//open a file with __FILE__ it will give file name.
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	int ch;
	//checking the file till it will reach the end.
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	//close the file pointer.
	fclose(fp);
	return 0;
}
