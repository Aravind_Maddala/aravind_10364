/*
 *
 * This program is for write some data into csv file and
 * read the data in csv file with comma separation.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//employee structure
typedef struct employee {
	int id;
	char name[20];
}emp;

int main(void) {

	FILE *fp=NULL;
	//count is for calculate number of employees.
	int cnt=3;
	if((fp = fopen("csv.csv","w+")) == NULL) {
		printf("Failed to open file\n");
		exit(0);
	}
	else 
		printf("OPENED SUCCESSFULLY\n");
	/*
	 * crate an pointer variable for structure and every time we overide it and 
	 * store the data into file approximately.
	 */
	emp *buf=(emp*)malloc(sizeof(emp));
	while(cnt!=0) {
		if(buf == NULL) {
			puts("Failed to memory allocation");
			exit(0);
		}
		puts("Enter id :");
		scanf("%d",&(buf->id));
		puts("Enter name :");
		scanf("%s",buf->name);
		fprintf(fp,"%d",buf->id);
		fputc(',',fp);
		fputs(buf->name,fp);
		fputc(10,fp);
		cnt--;
	}
	//free the buffer to avoid the memory leak.
	free(buf);
	//close the file pointer 
	fclose(fp);

	//open the file with read mode
	if((fp = fopen("csv.csv","r")) == NULL) {
		printf("Failed to open file\n");
		exit(0);
	}
	else 
		printf("OPENED SUCCESSFULLY\n");

	emp *temp=(emp*)malloc(sizeof(emp));

 	while(fscanf(fp,"%d %[^\n]",&temp->id,temp->name)!=EOF) {
			printf("id:%d ",temp->id);
			char *ptr = temp -> name;
			printf("Name : %s\n",(ptr+1));
	}
	free(temp);
	//close the file pointer.
	fclose(fp);
	return 0;
}
