/***********************************************************************************
 *  Aravind Maddala         10360       dvsaravindd@gmail.com       cell:9959989615
 *
 *  PURPOSE :
 *  This programe reads the elements from csv file and store in 2D matrix and
 *  prints the elements in the matrix
 *
 ***********************************************************************************/
#include<stdio.h>
int main() {
	FILE *fp1=NULL;
	int ch,count=0;
	if((fp1=fopen("matrix.csv","r")) == NULL) {
		printf("Failed to open the file.....\n");
		return -1;
	}
	else
		printf("File 1  Opened succesfully\n");

	while((ch = fgetc(fp1)) != -1 ) {
		if(ch==',' || ch==10){
		}else {
			count+=1;
		}
	}
	printf("Total %d elements in the CSV file : \n",count);
	rewind(fp1);
	int rows,columns;
	int i=0,j=0;
	printf("Enter the rows of a matrix\n");
	scanf("%d",&rows);
	printf("Enter the columns of a matrix\n");
	scanf("%d",&columns);
	while(rows*columns<count) {
		printf("The entered rows and columns product %d is less than total numners %d \n",rows*columns,count);
		printf("Enter the rows of a matrix\n");
		scanf("%d",&rows);
		printf("Enter the columns of a matrix\n");
		scanf("%d",&columns);
	}


	int matrix[rows][columns];

	while((ch = fgetc(fp1)) != -1) {
		if(ch==',' || ch==10){

			j+=1;
			if(j>=columns) {
				j=0;
				i+=1;
			}

		}else {
			matrix[i][j]=ch-48;
		}

	}
	//making remianing elemnts in the matrix 0,if rows*columns>count
	for(i;i<=rows;i++) {
		for(j;j<=columns;j++) {
			matrix[i][j]=0;
		}
		j=0;
	}
	//printing matrix elements
	printf(" The elemenets in the matrix wriiten from CSV file are :\n");

	for(i=0;i<rows;i++) {
		for(j=0;j<columns;j++) {
			printf("%d ",matrix[i][j]);
		}
		printf("\n");
	}




	return 0;
}
/*******************OUTPUT************************************
File 1  Opened succesfully
Total 20 elements in the CSV file :
Enter the rows of a matrix
5
Enter the columns of a matrix
5
 The elemenets in the matrix wriiten from CSV file are :
1 2 3 4 1
2 3 4 1 2
3 4 1 2 3
4 1 2 3 4
0 0 0 0 0
****************************************************************/
