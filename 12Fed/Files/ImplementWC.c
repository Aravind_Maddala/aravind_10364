/*
 *
 * This program is for creating user defined wc command.
 */
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	//initialized file pointer with null
	FILE *fp=NULL;
	if((fp=fopen(__FILE__,"r")) == NULL) {
		printf("Filed to open file\n");
		return -1;
	}
	printf("FILE OPEND SUCCESSFULLY\n");
	int ch;
	/*
	 * initializing the variables with 0 here 
	 * line_cnt is for counting the number of lines,
	 * word_cnt is for counting the number of words in the given file,
	 * char_cnt is for counting the number of charecter in given file.
	 */
	int line_cnt=0,word_cnt=0,char_cnt=0;
	while((ch=fgetc(fp))!=-1) {
		if(ch == ' ')
			word_cnt++;
		else if(ch == '\n')
			line_cnt++;
		else
			char_cnt++;
	}
	fseek(fp,0,SEEK_END);
	printf("Total Number of words :%d\n",word_cnt+line_cnt);
	printf("Total Number of Lines :%d\n",line_cnt);
	printf("Total Number of charecters :%d\n",char_cnt);
	printf("The size of File : %ld\n",ftell(fp));
	//close the file pointer
	fclose(fp);
	return 0;
}

