/***************************************************
  Aravind Maddala   Emp id:10364   cell No:9959989615
  ---------------

  purpose:
  ----------------
  Implement sorting Mechanisums selection and Insertion

****************************************************/
#include <iostream>
#include <cstdlib>
using namespace std;

class SortMechanisams {

private:
	int *array;
	int size;
public:
	SortMechanisams(int);
	void bubbleSort();
	void insertionSort();
//	friend swap(int*,int*);
	void printArray();
	void mergeSort(int,int,int);
};

//Fuction to swap the two elements
void swapvariables(int &left, int &right) { 
    int temp = left; 
    left = right; 
    right = temp; 
} 
  
SortMechanisams :: SortMechanisams(int size) {
	
	this->size = size;
	int *ptr = new int[size];
	cout<< "enter the Elements :"<<endl;
	for( int i=0; i< size ; i++) {
		cin >> ptr[i];
	}
	cout<<endl;
	array = ptr;
	
}
// A function to implement bubble sort 
void SortMechanisams  :: bubbleSort() { 
  
   int i, j; 
   for (i = 0; i < size-1; i++) {
  
       // Last i elements are already in place    
       for (j = 0; j < size-i-1; j++) { 
           if (array[j] > array[j+1]) 
              swapvariables(array[j],array[j+1]);
       }
   }	
} 

/* Function to sort an array using insertion sort*/
void SortMechanisams :: insertionSort() { 
   int i, key, j; 
   for (i = 1; i < size ; i++) { 
       key = array[i]; 
       j = i-1; 
  
       while (j >= 0 && array[j] > key) { 
           array[j+1] = array[j]; 
           j = j-1; 
       } 
       array[j+1] = key; 
   } 
} 
/* Function to print an array */
void SortMechanisams :: printArray() { 
    int i; 
    cout << "{";
    for (i=0; i < size; i++) 
        cout <<"'"<<array[i]<<"' "; 
    cout << "}"<<endl;
} 
 
int main(void) {
	int size;
	cout << "Enter size of Array :"<<endl;
	cin >> size;
	SortMechanisams object(size);
	do {
	cout << "\tPRESS"<<endl;
	cout << "************************"<<endl;
	cout << "1.BUBBLE SORT\n2.INSERTION SORT\n3.DISPLAY\n4.EXIT"<<endl;
	cout << "************************"<<endl;
	cout << "Enter Choice to perform"<<endl;
	int choice;
	cin >> choice;
	switch(choice) {
	
	   case 1 : object.bubbleSort();
		    break;
	   case 2 : object.insertionSort();
		    break;
	   case 3 : object.printArray();
		    break;
	   case 4 : exit(0);
	 	    
	
	   default : cout << "Invalid Choice"<<endl;
	}
	}while(1);
	return 0;
}
/*****************************************************
Output:
	PRESS
************************
1.BUBBLE SORT
2.INSERTION SORT
3.DISPLAY
4.EXIT
************************
Enter Choice to perform
3
{'7' '9' '0' '2' '1' }
	PRESS
************************
1.BUBBLE SORT
2.INSERTION SORT
3.DISPLAY
4.EXIT
************************
Enter Choice to perform
2
	PRESS
************************
1.BUBBLE SORT
2.INSERTION SORT
3.DISPLAY
4.EXIT
************************
Enter Choice to perform
3
{'0' '1' '2' '7' '9' }
	PRESS
************************
1.BUBBLE SORT
2.INSERTION SORT
3.DISPLAY
4.EXIT
************************
*****************************************************/
