/**************************************************************
	Aravind Maddala 	Emp id:10364	cell NO:9959989615
	---------------------
	
	Purpose:
	--------------------
	Create stack to Preform the Push and Pop operations.
	
**************************************************************/
#include <iostream>
#include <cstdlib>
#include <string.h>

using namespace std;


class Stack {

private :
	struct List {
		
	   int data;
	   //create self referential pointer 
	   struct List *next;
	};
	struct List *head=NULL;
	int length;
public:
	void pushData(int);
	void popData();
	void displayStackList();
};
/*
 Method for  Pushing the data into the stack 

*/
void Stack :: pushData(int data) {
    
    List *newNode = new List;
    List *temp; //temp for traversing 
    length++;
    
    if(newNode == NULL) {
    	cout << "Unable to Allocate Memory:"<<endl;
    }
        
    else {
        newNode->data = data; // Link the data part
        newNode->next = NULL; 
        if( head == NULL) {
	   head = newNode;
	}
        else {
	  temp = head;

          // Traverse to the last node
          while(temp->next != NULL)
              temp = temp->next;

          temp->next = newNode; // Link address part

          cout<<"DATA INSERTED SUCCESSFULLY"<<endl;
	}
    }

}
/*
 Method for pop the data into the stack

*/
void Stack :: popData() {
	
	List *temp,*prev;
	temp = head;
	if(head == NULL) {
	   cout << "The stack is Empty"<<endl;
	}
	while( temp ->next != NULL) {
		prev =temp;
		temp = temp -> next;
	}
	delete temp;
	prev->next = NULL;
	length--;
	cout << "POP THE DATA SUCCESSFULLY"<<endl;
} 
/*
  Display the data which is presented in stack
*/
void Stack :: displayStackList() {

	List *temp;
	temp = head;
	if (head == NULL) {
		cout <<"List is Empty"<<endl;
		exit(0);
	}
	while ( temp -> next != NULL ) {
	   
	   cout << temp -> data;
	   temp = temp -> next;
	   cout << "--->";
	}
	cout << temp ->data;
	cout<<endl<<endl;

}

int main(void) {
	//Stack class variable
	Stack Node;
	do {
	  int choice;
	  cout << "***********************"<<endl;
	  cout << "1.PUSH DATA\n2.POP DATA\n3.Display\n4.Exit"<<endl;
	  cout << "***********************"<<endl;
	  cout << "Enter the choice to preform the operation:"<<endl;
	  cin >> choice;	

	  switch(choice) {
	
	      case 1 : {
			 int data;
			 cout << "Enter the data to Push :"<<endl;
			 cin >> data;
			 Node.pushData(data);
		 	}
			break;
	      case 2 : {
			cout << "Pop the Last Element :"<<endl;
			Node.popData();	
		  	}
			break;
	      case 3 : Node.displayStackList();
			break;
	      case 4 : exit(0);
	      default :  cout << "Invalid choice "<<endl;
	  }
	} while(1);

	return 0;
}
/************************************************************
output:
***********************
1.PUSH DATA
2.POP DATA
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
3
5--->4--->3--->2--->1

***********************
1.PUSH DATA
2.POP DATA
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
2
Pop the Last Element :
POP THE DATA SUCCESSFULLY
***********************
1.PUSH DATA
2.POP DATA
3.Display
4.Exit
***********************
Enter the choice to preform the operation:
3
5--->4--->3--->2

***********************
1.PUSH DATA
2.POP DATA
3.Display
4.Exit
***********************
************************************************************/
